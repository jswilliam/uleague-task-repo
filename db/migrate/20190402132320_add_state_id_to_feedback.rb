# frozen_string_literal: true

class AddStateIdToFeedback < ActiveRecord::Migration[5.2]
  def change
    add_column :states, :feedback_id, :integer
    add_index  :states, :feedback_id
  end
end
