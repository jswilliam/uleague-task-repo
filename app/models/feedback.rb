# frozen_string_literal: true

class Feedback < ApplicationRecord
  validates :company_token, :priority, presence: true
  validates :priority, inclusion: { in: %w[minor major critical], message: 'must be minor, major, or critical' }

  has_one :state
  accepts_nested_attributes_for :state

  before_save :generate_number

  def generate_number
    feedback = Feedback.where(company_token: company_token).last
    self.number = (feedback.present? ? feedback.number + 1 : 1)
  end
end
