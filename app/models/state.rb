# frozen_string_literal: true

class State < ApplicationRecord
  belongs_to :feedback
end
