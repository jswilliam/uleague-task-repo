# frozen_string_literal: true

class StateSerializer < ActiveModel::Serializer
  attributes :id, :device, :os, :memory, :storage
end
