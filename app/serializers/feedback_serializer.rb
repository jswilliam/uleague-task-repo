# frozen_string_literal: true

class FeedbackSerializer < ActiveModel::Serializer
  attributes :id, :company_token, :number, :priority, :state
  has_one :state
end
