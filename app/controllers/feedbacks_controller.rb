# frozen_string_literal: true

class FeedbacksController < ApplicationController
  # POST /feedbacks
  def create
    @feedback = Feedback.new(feedback_params)

    if @feedback.save
      render json: @feedback, status: :created
    else
      render json: @feedback.errors, status: :unprocessable_entity
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def feedback_params
    params.require(:feedback).permit(:company_token, :priority,
                                     state_attributes: %i[device os memory storage])
  end
end
